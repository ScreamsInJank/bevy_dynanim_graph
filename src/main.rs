use eframe::egui;
use egui_plot::{Legend, Line, Plot, PlotPoints};

const ZERO_POINT: [f64;2] = [0.0,0.0];

fn main() -> eframe::Result{
    env_logger::init(); // Log to stderr (if you run with `RUST_LOG=debug`).
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_inner_size([350.0, 200.0]),
        ..Default::default()
    };

    let mut f_a: f32 = 1.0;
    let mut z_a: f32 = 0.5;
    let mut r_a: f32 = 2.0;
    let mut f_b: f32 = 1.0;
    let mut z_b: f32 = 0.5;
    let mut r_b: f32 = 2.0;

    let mut a_solver: Solver = Solver::Microstep;
    let mut b_solver: Solver = Solver::Default;

    let mut sample_freq = 60.0;
    let mut time_limit: f32 = 6.0;

    eframe::run_simple_native("My egui App", options, move |ctx, _frame| {
        egui::CentralPanel::default().show(ctx, |ui| {
            let dt: f32 = 1.0/sample_freq;

            ui.heading("General Configuration:");
            ui.heading("System A: Semi Implicit Euler");
            ui.horizontal(|ui| {
                ui.label("Sample Rate (Hz):");
                ui.add(egui::DragValue::new(&mut sample_freq).range(1.0..=f32::INFINITY));
                ui.label("Time Limit:");
                ui.add(egui::DragValue::new(&mut time_limit).range(1.0..=f32::INFINITY));
            });
            
            let mut a_input_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            let mut a_d_input_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            let mut a_output_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            let mut a_d_output_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            ui.horizontal(|ui| {
                ui.label("Frequency:");
                ui.add(egui::Slider::new(&mut f_a, 0.0..=10.0));
                ui.label("Damping:");
                ui.add(egui::Slider::new(&mut z_a, 0.0..=2.0));
                ui.label("Response:");
                ui.add(egui::Slider::new(&mut r_a, -4.0..=4.0));
                ui.label("Solver:");
                egui::ComboBox::new("a_solver", "")
                .selected_text(format!("{a_solver:?}"))
                .show_ui(ui, |ui| {
                    ui.selectable_value(&mut a_solver, Solver::Default, "Default");
                    ui.selectable_value(&mut a_solver, Solver::Microstep, "Microstep");
                });
            });
            let a_params = bevy_dynanim::dynamics::core::util::DynamicsTargetParams::new(f_a, z_a, r_a);
            
            ui.heading("Plot:");

            let a_plot = Plot::new("a_plot").view_aspect(4.0).legend(Legend::default());

            {
                let mut a_input:    f32 = 0.0;
                let mut a_d_input:  f32;
                let mut a_output:   f32 = 0.0;
                let mut a_d_output: f32 = 0.0;

                // Compute plot values
                for i in 0..(sample_freq*time_limit).floor() as usize {
                    let time = dt*(i as f32);
    
                    // Calculate input based on step function
                    let a_prev_input = a_input;
                    a_input = if time > 1.0 { 1.0 } else { 0.0 };
                    // Derive input with respect to time
                    a_d_input = (a_input - a_prev_input)/dt;
                    // Run the solver
                    match a_solver {
                        Solver::Default => bevy_dynanim::dynamics::solvers::default::dynamics_step::<f32>(dt, &a_params, a_input, a_d_input, &mut a_output, &mut a_d_output),
                        Solver::Microstep => bevy_dynanim::dynamics::solvers::microstep::dynamics_step::<f32>(dt, &a_params, a_input, a_d_input, &mut a_output, &mut a_d_output),
                    }

                    let time = time as f64;
                    // Push values to buffers
                    a_input_buffer[i] = [time, a_input as f64];
                    a_d_input_buffer[i] = [time, a_d_input as f64];
                    a_output_buffer[i] = [time, a_output as f64];
                    a_d_output_buffer[i] = [time, a_d_output as f64];
                    
                }
            }

            let a_inner = a_plot.show(ui, |plot_ui| {
                plot_ui.line(Line::new(PlotPoints::from(a_input_buffer.clone())).name("A.x"));
                plot_ui.line(Line::new(PlotPoints::from(a_d_input_buffer)).name("A.x\'"));
                plot_ui.line(Line::new(PlotPoints::from(a_output_buffer.clone())).name("A.y"));
                plot_ui.line(Line::new(PlotPoints::from(a_d_output_buffer)).name("A.y\'"));
            });

            ui.heading("System B: Implicit Euler");
            let mut b_input_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            let mut b_d_input_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            let mut b_output_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            let mut b_d_output_buffer: Vec<[f64;2]> = vec![ZERO_POINT;(sample_freq*time_limit).floor() as usize];
            
            ui.horizontal(|ui| {
                ui.label("Frequency:");
                ui.add(egui::Slider::new(&mut f_b, 0.0..=10.0));
                ui.label("Damping:");
                ui.add(egui::Slider::new(&mut z_b, 0.0..=2.0));
                ui.label("Response:");
                ui.add(egui::Slider::new(&mut r_b, -4.0..=4.0));
                ui.label("Solver:");
                egui::ComboBox::new("b_solver", "")
                .selected_text(format!("{b_solver:?}"))
                .show_ui(ui, |ui| {
                    ui.selectable_value(&mut b_solver, Solver::Default, "Default");
                    ui.selectable_value(&mut b_solver, Solver::Microstep, "Microstep");
                });
            });


            let b_params = bevy_dynanim::dynamics::core::util::DynamicsTargetParams::new(f_b, z_b, r_b);
            
            let b_plot = Plot::new("b_plot").view_aspect(4.0).legend(Legend::default());

            {
                let mut b_input:    f32 = 0.0;
                let mut b_d_input:  f32;
                let mut b_output:   f32 = 0.0;
                let mut b_d_output: f32 = 0.0;

                // Compute plot values
                for i in 0..(sample_freq*time_limit).floor() as usize {
                    let time = dt*(i as f32);
    
                    // Calculate input based on step function
                    let b_prev_input = b_input;
                    b_input = if time > 1.0 { 1.0 } else { 0.0 };
                    // Derive input with respect to time
                    b_d_input = (b_input - b_prev_input)/dt;
                    // Run the solver
                    match b_solver {
                        Solver::Default => bevy_dynanim::dynamics::solvers::default::dynamics_step::<f32>(dt, &b_params, b_input, b_d_input, &mut b_output, &mut b_d_output),
                        Solver::Microstep => bevy_dynanim::dynamics::solvers::microstep::dynamics_step::<f32>(dt, &b_params, b_input, b_d_input, &mut b_output, &mut b_d_output),
                    }

                    let time = time as f64;
                    // Push values to buffers
                    b_input_buffer[i] = [time, b_input as f64];
                    b_d_input_buffer[i] = [time, b_d_input as f64];
                    b_output_buffer[i] = [time, b_output as f64];
                    b_d_output_buffer[i] = [time, b_d_output as f64];
                    
                }
            }

            let b_inner = b_plot.show(ui, |plot_ui| {
                plot_ui.line(Line::new(PlotPoints::from(b_input_buffer)).name("B.x"));
                plot_ui.line(Line::new(PlotPoints::from(b_d_input_buffer)).name("B.x\'"));
                plot_ui.line(Line::new(PlotPoints::from(b_output_buffer.clone())).name("B.y"));
                plot_ui.line(Line::new(PlotPoints::from(b_d_output_buffer)).name("B.y\'"));
            });
            ui.heading("Comparison: Step Response of A vs B");
            let comparison_plot = Plot::new("c_plot").view_aspect(4.0).legend(Legend::default());
            let comparison_inner = comparison_plot.show(ui, |plot_ui| {
                plot_ui.line(Line::new(PlotPoints::from(a_input_buffer)).name("x"));
                plot_ui.line(Line::new(PlotPoints::from(a_output_buffer)).name("A.y"));
                plot_ui.line(Line::new(PlotPoints::from(b_output_buffer)).name("B.y"));
            });
        });
    })
}

#[derive(PartialEq, Debug)]
enum Solver {
    Default,
    Microstep
}

// ui.add_enabled_ui(false, |ui| {
//     let dt: f32 = 1.0/60.0;
//     let mut input: f32 = 0.0;
//     let mut prev_input: f32 = 0.0;
//     let mut d_input: f32 = 0.0;
//     let mut output: f32 = 0.0;
//     let mut d_output: f32 = 0.0;
//     let default_solver_points: PlotPoints = (0..1000).map(|i| {
//         let time = dt*(i as f32);
//         input = if time > 1.0 { 1.0 } else { 0.0 };
//         d_input = (input-prev_input)/dt;
//         crate::dynamics::solvers::default::dynamics_step(dt, self, input, d_input, &mut output, &mut d_output);
//         prev_input = input;
//         [time as f32, output as f32]
//     }).collect();
//     let default_solver_line = Line::new(default_solver_points);
//     Plot::new("my_plot").view_aspect(2.0).show(ui, |plot_ui| plot_ui.line(default_solver_line)).response.changed();
// });